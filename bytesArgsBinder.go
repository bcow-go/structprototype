package structprototype

import (
	"fmt"
	"reflect"
)

type BytesArgsBinder reflect.Value

func (binder BytesArgsBinder) Bind(input interface{}) error {
	buf, ok := input.([]byte)
	if !ok {
		return fmt.Errorf("cannot bind type %T from input", input)
	}
	v := string(buf)

	return StringArgsBinder(reflect.Value(binder)).Bind(v)
}

func BuildBytesArgsBinder(rv reflect.Value) ValueBinder {
	return BytesArgsBinder(rv)
}
