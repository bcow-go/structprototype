package structprototype

import (
	"net/url"
	"reflect"
	"time"
)

const (
	errStringValueLength = 24
)

var (
	typeOfDuration = reflect.TypeOf(time.Nanosecond)
	typeOfUrl      = reflect.TypeOf(url.URL{})
	typeOfTime     = reflect.TypeOf(time.Time{})
)

const (
	RequiredFlag = "required"
)

type (
	BuildValueBinderFunc func(rv reflect.Value) ValueBinder
	StructTagResolver    func(fieldname, token string) (*StructTag, error)

	KeyValuePairIterator interface {
		Iterate() <-chan KeyValuePair
	}

	KeyValuePair struct {
		Key   string
		Value interface{}
	}

	PrototypeField interface {
		Name() string
		Desc() string
		Index() int
		Flags() []string
		HasFlag(v string) bool
	}

	BindingProvider interface {
		BeforeBind(context *PrototypeContext) error
		BindField(field PrototypeField, rv reflect.Value) error
		AfterBind(context *PrototypeContext) error
	}

	PrototypifyConfig struct {
		TagName              string
		BuildValueBinderFunc BuildValueBinderFunc
		StructTagResolver    StructTagResolver
	}

	StructTag struct {
		Name  string
		Flags []string
		Desc  string
	}

	StringSet interface {
		Append(values ...string)
		Clone() StringSet
		Get(index int) (string, bool)
		Has(name string) bool
		Indexof(name string) int
		IsEmpty() bool
		Iterate() <-chan string
		Len() int
		Remove(name string) bool
		RemoveIndex(index int) (bool, string)
	}

	ValueBinder interface {
		Bind(v interface{}) error
	}
)

func BuildNilBinder(rv reflect.Value) ValueBinder { return nil }
