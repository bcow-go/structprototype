package structprototype

type FieldInfo struct {
	name  string
	desc  string
	index int
	flags stringSortedSet
}

func (info *FieldInfo) Name() string {
	return info.name
}

func (info *FieldInfo) Desc() string {
	return info.desc
}

func (info *FieldInfo) Index() int {
	return info.index
}

func (info *FieldInfo) Flags() []string {
	return info.flags
}

func (info *FieldInfo) HasFlag(v string) bool {
	return info.flags.has(v)
}

func (info *FieldInfo) appendFlags(values ...string) {
	if len(values) > 0 {
		for _, v := range values {
			if len(v) == 0 {
				continue
			}
			info.flags.append(v)
		}
	}
}
