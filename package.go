package structprototype

func Bind(v interface{}, options *PrototypifyConfig, provider BindingProvider) error {
	if v == nil {
		panic("specified argument 'v' cannot be nil")
	}
	if options == nil {
		panic("specified argument 'options' cannot be nil")
	}
	if provider == nil {
		panic("specified argument 'provider' cannot be nil")
	}

	if options.BuildValueBinderFunc == nil {
		options.BuildValueBinderFunc = BuildNilBinder
	}

	prototype, err := Prototypify(v, options)
	if err != nil {
		return err
	}

	binder, err := NewPrototypeBinder(prototype, provider)
	if err != nil {
		return err
	}

	err = binder.Bind()
	if err != nil {
		return err
	}
	return nil
}

func Prototypify(v interface{}, conf *PrototypifyConfig) (*Prototype, error) {
	if v == nil {
		panic("specified argument 'v' cannot be nil")
	}
	if conf == nil {
		panic("specified argument 'config' cannot be nil")
	}

	r := &resolver{
		tagName:          conf.TagName,
		buildValueBinder: conf.BuildValueBinderFunc,
		resolveTag:       conf.StructTagResolver,
	}
	if r.resolveTag == nil {
		r.resolveTag = StdStructTagResolver
	}
	prototype, err := r.resolve(v)
	if err != nil {
		return nil, err
	}
	return prototype, nil
}
