package structprototype

import (
	"reflect"
)

type Prototype struct {
	value reflect.Value

	fields         map[string]*FieldInfo
	requiredFields stringSortedSet

	buildValueBinder BuildValueBinderFunc
}

func (prototype *Prototype) BindValues(values KeyValuePairIterator) error {
	if prototype == nil {
		return nil
	}

	var requiredFields = prototype.requiredFields.clone()

	// mapping values
	for p := range values.Iterate() {
		field, val := p.Key, p.Value
		if val != nil {
			binder := prototype.makeFieldBinder(prototype.value, field)
			if binder != nil {
				err := binder.Bind(val)
				if err != nil {
					return &FieldBindingError{field, val, err}
				}

				index := requiredFields.indexof(field)
				if index != -1 {
					// eliminate the field from slice if found
					requiredFields.removeIndex(index)
				}
			}
		}
	}

	// check if the requiredFields still have fields don't be set
	if !requiredFields.isEmpty() {
		field, _ := requiredFields.get(0)
		return &MissingRequiredFieldError{field, nil}
	}

	return nil
}

func (prototype *Prototype) makeFieldBinder(rv reflect.Value, name string) ValueBinder {
	if prototype == nil {
		return nil
	}
	if f, ok := prototype.fields[name]; ok {
		binder := prototype.buildValueBinder(rv.Field(f.index))
		return binder
	}
	return nil
}

func createPrototype(value reflect.Value) *Prototype {
	prototype := Prototype{
		value:  value,
		fields: make(map[string]*FieldInfo),
	}
	return &prototype
}
