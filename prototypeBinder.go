package structprototype

type PrototypeBinder struct {
	prototype *Prototype
	provider  BindingProvider
}

func NewPrototypeBinder(prototype *Prototype, provider BindingProvider) (*PrototypeBinder, error) {
	if prototype == nil {
		panic("specified argument 'prototype' cannot be nil")
	}
	if provider == nil {
		panic("specified argument 'provider' cannot be nil")
	}

	processor := PrototypeBinder{
		prototype: prototype,
		provider:  provider,
	}
	return &processor, nil
}

func (p *PrototypeBinder) Bind() error {
	var err error

	context, err := createPrototypeContext(p.prototype)
	if err != nil {
		return err
	}

	if err = p.provider.BeforeBind(context); err != nil {
		return err
	}

	if err = p.bind(); err != nil {
		return err
	}

	if err = p.provider.AfterBind(context); err != nil {
		return err
	}

	return nil
}

func (p *PrototypeBinder) bind() error {
	var (
		prototype = p.prototype
		provider  = p.provider
	)
	for _, field := range prototype.fields {
		err := provider.BindField(field, prototype.value.Field(field.index))
		if err != nil {
			return err
		}
	}
	return nil
}
