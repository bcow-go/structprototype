package structprototype

import "reflect"

type PrototypeContext Prototype

func (ctx *PrototypeContext) Target() reflect.Value {
	return ctx.value
}

func (ctx *PrototypeContext) Field(name string) PrototypeField {
	if field, ok := ctx.fields[name]; ok {
		return field
	}
	return nil
}

func (ctx *PrototypeContext) AllFields() []string {
	var fields []string = make([]string, len(ctx.fields))
	for _, v := range ctx.fields {
		fields[v.index] = v.name
	}
	return fields
}

func (ctx *PrototypeContext) AllRequiredFields() StringSet {
	return ctx.requiredFields
}

func (ctx *PrototypeContext) IsRequireField(name string) bool {
	field := ctx.Field(name)
	if field != nil {
		return field.HasFlag(RequiredFlag)
	}
	return false
}

func (ctx *PrototypeContext) CheckIfMissingRequiredFields(fieldVisitFunc func() <-chan string) error {
	if ctx.requiredFields.isEmpty() {
		return nil
	}

	var requiredFields = ctx.requiredFields.clone()

	for field := range fieldVisitFunc() {
		index := requiredFields.indexof(field)
		if index != -1 {
			requiredFields.removeIndex(index)
		}
	}

	if !requiredFields.isEmpty() {
		field, _ := requiredFields.get(0)
		return &MissingRequiredFieldError{field, nil}
	}
	return nil
}

func createPrototypeContext(prototype *Prototype) (*PrototypeContext, error) {
	context := PrototypeContext(*prototype)
	return &context, nil
}
