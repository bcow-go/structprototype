package structprototype

import (
	"reflect"
	"testing"
)

func TestPrototypeContext(t *testing.T) {
	c := mockCharacter{}
	prototype, err := Prototypify(&c, &PrototypifyConfig{
		TagName:              "demo",
		BuildValueBinderFunc: BuildStringArgsBinder,
	})
	if err != nil {
		t.Error(err)
	}

	context, err := createPrototypeContext(prototype)
	if err != nil {
		t.Error(err)
	}

	expectedFields := []string{"NAME", "AGE", "ALIAS", "DATE_OF_BIRTH", "REMARK", "NUMBERS"}
	if !reflect.DeepEqual(expectedFields, context.AllFields()) {
		t.Errorf("assert 'StructPrototypeContext.AllFields()':: expected '%#v', got '%#v'", expectedFields, context.AllFields())
	}
	expectedRequiredFields := stringSortedSet([]string{"AGE", "NAME"})
	if !reflect.DeepEqual(expectedRequiredFields, context.AllRequiredFields()) {
		t.Errorf("assert 'StructPrototypeContext.AllRequiredFields()':: expected '%#v', got '%#v'", expectedRequiredFields, context.AllRequiredFields())
	}

	{
		field := context.Field("NAME")
		if field == nil {
			t.Errorf("assert 'StructPrototypeContext.Field(\"NAME\")':: expected not nil, got '%#v'", field)
		}
		if field.Name() != "NAME" {
			t.Errorf("assert 'StructPrototypeField.Name()':: expected '%#v', got '%#v'", "NAME", field.Name())
		}
		if field.Index() != 0 {
			t.Errorf("assert 'StructPrototypeField.Index()':: expected '%#v', got '%#v'", "NAME", field.Name())
		}
		expectedFlags := []string{"required"}
		if !reflect.DeepEqual(expectedFlags, field.Flags()) {
			t.Errorf("assert 'StructPrototypeField.Flags()':: expected '%#v', got '%#v'", expectedFlags, field.Flags())
		}
	}

	if !context.IsRequireField("NAME") {
		t.Errorf("assert 'StructPrototypeContext.IsRequireField(\"NAME\")':: expected '%#v', got '%#v'", expectedRequiredFields, context.IsRequireField("NAME"))
	}
	if context.IsRequireField("unknown") {
		t.Errorf("assert 'StructPrototypeContext.IsRequireField(\"unknown\")':: expected '%#v', got '%#v'", expectedRequiredFields, context.IsRequireField("unknown"))
	}

	// TODO: test context.ChechIfMissingRequireFields
}
