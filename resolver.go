package structprototype

import (
	"fmt"
	"reflect"
)

type resolver struct {
	tagName          string
	buildValueBinder BuildValueBinderFunc
	resolveTag       StructTagResolver
}

func (r *resolver) resolve(v interface{}) (*Prototype, error) {
	var rv reflect.Value
	switch v.(type) {
	case reflect.Value:
		rv = v.(reflect.Value)
	default:
		rv = reflect.ValueOf(v)
	}

	if !rv.IsValid() {
		return nil, fmt.Errorf("specified argument 'v' is invalid")
	}

	for i := 0; true; i++ {
		if i >= 32 {
			return nil, fmt.Errorf("exceed maximum processing calls")
		}
		switch rv.Kind() {
		case reflect.Struct:
			info, err := r.resolveAllField(rv)
			if err != nil {
				return nil, err
			}
			return info, nil
		case reflect.Ptr:
			if rv.IsNil() {
				rv = reflect.New(rv.Type().Elem())
			}
			rv = rv.Elem()
		default:
			return nil, fmt.Errorf("specified argument 'v' must be pointer to struct")
		}
	}
	return nil, nil
}

func (r *resolver) resolveAllField(rv reflect.Value) (*Prototype, error) {
	var prototype = createPrototype(rv)
	if r.buildValueBinder == nil {
		return nil, fmt.Errorf("missing BuildValueBinderFunc")
	}
	prototype.buildValueBinder = r.buildValueBinder
	t := rv.Type()
	count := t.NumField()
	for i := 0; i < count; i++ {
		fieldname := t.Field(i).Name
		token := t.Field(i).Tag.Get(r.tagName)
		tag, err := r.resolveTag(fieldname, token)
		if err != nil {
			return nil, err
		}
		if tag != nil {
			field := &FieldInfo{
				name:  tag.Name,
				index: i,
				desc:  tag.Desc,
			}
			field.appendFlags(tag.Flags...)

			prototype.fields[tag.Name] = field
			if field.HasFlag(RequiredFlag) {
				prototype.requiredFields.append(tag.Name)
			}
		}
	}
	return prototype, nil
}
