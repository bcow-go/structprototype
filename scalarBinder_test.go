package structprototype

import (
	"reflect"
	"testing"
	"time"
)

func TestScalarValueBinder_WithBool(t *testing.T) {
	var v bool = false
	var input = []byte("true")

	rv := reflect.ValueOf(&v).Elem()
	binder := ScalarBinder(rv)
	binder.Bind(input)
	if v != true {
		t.Errorf("assert 'v':: expected '%#v', got '%#v'", true, v)
	}
}

func TestScalarValueBinder_WithInt(t *testing.T) {
	var v int = 0
	var input = []byte("1")

	rv := reflect.ValueOf(&v).Elem()
	binder := ScalarBinder(rv)
	binder.Bind(input)
	if v != 1 {
		t.Errorf("assert 'v':: expected '%#v', got '%#v'", 1, v)
	}
}

func TestScalarValueBinder_WithString(t *testing.T) {
	var v string = ""
	var input = []byte("foo")

	rv := reflect.ValueOf(&v).Elem()
	binder := ScalarBinder(rv)
	binder.Bind(input)
	if v != "foo" {
		t.Errorf("assert 'v':: expected '%#v', got '%#v'", "foo", v)
	}
}

func TestScalarValueBinder_WithDuration(t *testing.T) {
	var v time.Duration
	var input = "1m2s"

	rv := reflect.ValueOf(&v).Elem()
	binder := ScalarBinder(rv)
	err := binder.Bind(input)
	if err != nil {
		t.Error(err)
	}

	expected, _ := time.ParseDuration("1m2s")
	if v != expected {
		t.Errorf("assert 'v':: expected '%#v', got '%#v'", expected, v)
	}
}
