package structprototype

import (
	"reflect"
	"testing"
	"time"
)

func TestStringArgValueBinder_WithBool(t *testing.T) {
	var v bool = false
	var input = "true"

	rv := reflect.ValueOf(&v).Elem()
	binder := StringArgsBinder(rv)
	err := binder.Bind(input)
	if err != nil {
		t.Error(err)
	}
	if v != true {
		t.Errorf("assert 'character.DatOfBirth':: expected '%#v', got '%#v'", true, v)
	}
}

func TestStringArgValueBinder_WithInt(t *testing.T) {
	var v int = 0
	var input = "1"

	rv := reflect.ValueOf(&v).Elem()
	binder := StringArgsBinder(rv)
	err := binder.Bind(input)
	if err != nil {
		t.Error(err)
	}
	if v != 1 {
		t.Errorf("assert 'character.DatOfBirth':: expected '%#v', got '%#v'", 1, v)
	}
}

func TestStringArgValueBinder_WithString(t *testing.T) {
	var v string = ""
	var input = "foo"

	rv := reflect.ValueOf(&v).Elem()
	binder := StringArgsBinder(rv)
	binder.Bind(input)
	if v != "foo" {
		t.Errorf("assert 'v':: expected '%#v', got '%#v'", "foo", v)
	}
}

func TestStringArgValueBinder_WithDuration(t *testing.T) {
	var v time.Duration
	var input = "1m2s"

	rv := reflect.ValueOf(&v).Elem()
	binder := StringArgsBinder(rv)
	err := binder.Bind(input)
	if err != nil {
		t.Error(err)
	}

	expected, _ := time.ParseDuration("1m2s")
	if v != expected {
		t.Errorf("assert 'v':: expected '%#v', got '%#v'", expected, v)
	}
}

func TestStringArgValueBinder_WithStringArray(t *testing.T) {
	var v []string
	var input = "alice,bob,carlo,david,frank,george"

	rv := reflect.ValueOf(&v).Elem()
	binder := StringArgsBinder(rv)
	err := binder.Bind(input)
	if err != nil {
		t.Error(err)
	}

	expected := []string{"alice", "bob", "carlo", "david", "frank", "george"}
	if !reflect.DeepEqual(v, expected) {
		t.Errorf("assert 'v':: expected '%#v', got '%#v'", expected, v)
	}
}

func TestStringArgValueBinder_WithIntArray(t *testing.T) {
	var v []int
	var input = "1,1,2,3,5,8,13"

	rv := reflect.ValueOf(&v).Elem()
	binder := StringArgsBinder(rv)
	err := binder.Bind(input)
	if err != nil {
		t.Error(err)
	}

	expected := []int{1, 1, 2, 3, 5, 8, 13}
	if !reflect.DeepEqual(v, expected) {
		t.Errorf("assert 'v':: expected '%#v', got '%#v'", expected, v)
	}
}
