package structprototype

import (
	"reflect"
	"sort"
)

var (
	emptyStringSortedSet = stringSortedSet(nil)
)

type stringSortedSet []string

func (s *stringSortedSet) append(values ...string) {
	set := *s
	if len(values) > 0 {
		for _, v := range values {
			i := sort.SearchStrings(set, v)
			var existed = false
			if i < len(set) {
				existed = set[i] == v
			}
			if !existed {
				container := set
				if i < len(container) {
					container = append(container, "")
					copy(container[i+1:], container[i:])
					container[i] = v
				} else {
					container = append(container, v)
				}
				set = container
				*s = set
			}
		}
	}
}

func (s *stringSortedSet) clone() *stringSortedSet {
	set := *s
	if !reflect.ValueOf(set).IsZero() {
		var container = make([]string, len(set))
		copy(container, set)
		cloned := stringSortedSet(container)
		return &cloned
	}
	return &emptyStringSortedSet
}

func (s *stringSortedSet) get(index int) (string, bool) {
	if !s.isEmpty() {
		set := *s
		if index >= 0 && index < len(set) {
			return set[index], true
		}
	}
	return "", false
}

func (s *stringSortedSet) has(v string) bool {
	if s.isEmpty() {
		return false
	}

	return -1 != s.indexof(v)
}

func (s *stringSortedSet) indexof(v string) int {
	if !s.isEmpty() {
		set := *s
		if len(set) > 0 {
			i := sort.SearchStrings(set, v)
			if i < len(set) {
				if set[i] == v {
					return i
				}
			}
		}
	}
	return -1
}

func (s *stringSortedSet) isEmpty() bool {
	return len(*s) == 0
}

func (s *stringSortedSet) len() int {
	if s.isEmpty() {
		return 0
	}

	return len(*s)
}

func (s *stringSortedSet) removeIndex(index int) (bool, string) {
	if !s.isEmpty() {
		set := *s
		if index >= 0 && index < len(set) {
			value := set[index]
			copy(set[index:], set[index+1:])
			set = set[:len(set)-1]
			*s = set

			return true, value
		}
	}
	return false, ""
}

func (s stringSortedSet) Append(values ...string) {
	s.append(values...)
}

func (s stringSortedSet) Clone() StringSet {
	return s.clone()
}

func (s stringSortedSet) Get(index int) (string, bool) {
	return s.get(index)
}

func (s stringSortedSet) Has(name string) bool {
	return s.has(name)
}

func (s stringSortedSet) Indexof(name string) int {
	return s.indexof(name)
}

func (s stringSortedSet) IsEmpty() bool {
	return s.isEmpty()
}

func (s stringSortedSet) Iterate() <-chan string {
	if !s.isEmpty() {
		c := make(chan string, 1)
		go func() {
			for _, v := range s {
				c <- v
			}
			close(c)
		}()
		return c
	}
	return nil
}

func (s stringSortedSet) Len() int {
	return s.len()
}

func (s stringSortedSet) Remove(v string) bool {
	if !s.isEmpty() {
		index := s.indexof(v)
		deleted, _ := s.removeIndex(index)
		return deleted
	}
	return false
}

func (s stringSortedSet) RemoveIndex(index int) (bool, string) {
	return s.removeIndex(index)
}
