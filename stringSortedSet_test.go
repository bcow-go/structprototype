package structprototype

import (
	"reflect"
	"testing"
)

func TestSortedSet_WithEmptySet(t *testing.T) {
	var set stringSortedSet

	// test isEmpty()
	{
		if set.isEmpty() != true {
			t.Errorf("assert 'isEmpty()':: expected '%+v', got '%+v'", true, set.isEmpty())
		}
	}
	// test count
	{
		if set.len() != 0 {
			t.Errorf("assert 'len()':: expected '%+v', got '%+v'", 0, set.len())
		}
	}
	// test get
	{
		v, found := set.get(0)
		if found != false {
			t.Errorf("assert found of 'get(0)':: expected '%+v', got '%+v'", false, found)
		}
		if v != "" {
			t.Errorf("assert value of 'get(0)':: expected '%+v', got '%+v'", "", v)
		}
	}
	// test indexOf
	{
		index := set.indexof("unknown")
		if index != -1 {
			t.Errorf("assert 'indexof()':: expected '%+v', got '%+v'", -1, index)
		}
	}
	// test contains
	{
		existed := set.has("unknown")
		if existed != false {
			t.Errorf("assert 'has()':: expected '%+v', got '%+v'", false, existed)
		}
	}
	// test clone
	{
		cloned := set.clone()
		if !reflect.DeepEqual(*cloned, set) {
			t.Errorf("assert 'clone()':: expected '%#v', got '%#v'", set, *cloned)
		}
	}
	// test append
	{
		set.append("bob", "alice")
		expected := []string{"alice", "bob"}
		if !reflect.DeepEqual(expected, []string(set)) {
			t.Errorf("assert 'append()':: expected '%#v', got '%#v'", expected, []string(set))
		}
	}
}

func TestSortedSet_WithSampleSet(t *testing.T) {
	var set stringSortedSet = []string{"bob", "georgy"}

	// test isEmpty()
	{
		if set.isEmpty() != false {
			t.Errorf("assert 'isEmpty()':: expected '%+v', got '%+v'", false, set.isEmpty())
		}
	}
	// test count
	{
		if set.len() != 2 {
			t.Errorf("assert 'len()':: expected '%+v', got '%+v'", 2, set.len())
		}
	}
	// test get
	{
		v, found := set.get(0)
		if found != true {
			t.Errorf("assert found of 'get(0)':: expected '%+v', got '%+v'", true, found)
		}
		if v != "bob" {
			t.Errorf("assert value of 'get(0)':: expected '%+v', got '%+v'", "bob", v)
		}
	}
	// test indexOf
	{
		index := set.indexof("georgy")
		if index != 1 {
			t.Errorf("assert 'indexof()':: expected '%+v', got '%+v'", 1, index)
		}
	}
	// test contains
	{
		existed := set.has("bob")
		if existed != true {
			t.Errorf("assert 'has()':: expected '%+v', got '%+v'", true, existed)
		}
	}
	// test clone
	{
		cloned := set.clone()
		if !reflect.DeepEqual(*cloned, set) {
			t.Errorf("assert 'clone()':: expected '%#v', got '%#v'", set, *cloned)
		}
	}
	// test append
	{
		set.append("bob", "alice")
		expected := []string{"alice", "bob", "georgy"}
		if !reflect.DeepEqual(expected, []string(set)) {
			t.Errorf("assert 'append()':: expected '%#v', got '%#v'", expected, []string(set))
		}
	}
	// test removeIndex
	{
		found, value := set.removeIndex(8)
		if found != false {
			t.Errorf("assert found of 'removeIndex()':: expected '%#v', got '%#v'", false, found)
		}
		if value != "" {
			t.Errorf("assert value of 'removeIndex()':: expected '%#v', got '%#v'", "", value)
		}
		expected := []string{"alice", "bob", "georgy"}
		if !reflect.DeepEqual(expected, []string(set)) {
			t.Errorf("assert 'stringSortedSet':: expected '%#v', got '%#v'", expected, []string(set))
		}
	}
	// test removeIndex
	{
		found, value := set.removeIndex(2)
		if found != true {
			t.Errorf("assert found of 'removeIndex()':: expected '%#v', got '%#v'", true, found)
		}
		if value != "georgy" {
			t.Errorf("assert value of 'removeIndex()':: expected '%#v', got '%#v'", "georgy", value)
		}
		expected := []string{"alice", "bob"}
		if !reflect.DeepEqual(expected, []string(set)) {
			t.Errorf("assert 'stringSortedSet':: expected '%#v', got '%#v'", expected, []string(set))
		}
	}
}
